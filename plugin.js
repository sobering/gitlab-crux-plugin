var m = require('mithril');
var fetchival = require('fetchival');

var gitLabLogo = require('./img/gitlab-logo.png');

var Plugin = {};

Plugin.controller = function(props) {
  this.data = m.prop([]);
  this.interval = null;
  var _this = this;

  this.fetchData = function () {
    fetchival('gitlab/activity').get().then(function (json) {
      _this.data(json);
      m.redraw();
    });
  };

  this.getData = function() {
    _this.interval = setInterval(function () {
      _this.fetchData();
    }, 60000);
  };

  this.config = function (el, isInit, ctx) {
    if (!isInit) {
      _this.getData();
      _this.fetchData();
    }

    ctx.onunload = function () {
      clearInterval(_this.interval);
    };
  };
};

Plugin.view = function(ctrl, props) {
  var style = {color: props.fontColor, backgroundColor: props.tileColor};
  return m('.Plugin', {config: ctrl.config, style: style}, [
    m('.GitLabActivityStream', [
      m('h2.GitLabActivityStream-header', 'GitLab Activity'),
      m('ul.GitLabActivityStream-list', [
        ctrl.data().map(function (data) {
          return m('li', [
            m('h3.GitLabActivityStream-title', data.title),
            m('span.GitLabActivityStream-dateTime', data.updated)
          ]
        )}),
      ])
    ]),
    m('.fader')
  ]);
};

module.exports = Plugin;
