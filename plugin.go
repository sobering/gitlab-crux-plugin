package gitlabplugin

import (
	"errors"
	"fmt"
	"log"
	"os"

	"git-lab.boldapps.net/proximity/crux/plugin"

	gl "github.com/plouc/go-gitlab-client"

	"github.com/labstack/echo"
)

var gitlab *gl.Gitlab

const gitLabBasePath = "https://git-lab.boldapps.net"
const gitLabAPIPath = "/api/v3"

// Plugin ...
func Plugin(dashboard *plugin.Crux) {

	// Do setup and initialization of resources here.
	gitlab = gl.NewGitlab(gitLabBasePath, gitLabAPIPath, os.Getenv("GITLAB_TOKEN"))
	if gitlab == nil {
		log.Fatal(errors.New("There was an issue communicating with Gitlab"))
	}

	dashboard.Router.Get("/gitlab/activity", activityRefresh)
	dashboard.Router.Get("/gitlab/projects", projectsRefresh)
	dashboard.Router.Get("/gitlab/team", teamRefresh)
	dashboard.Router.Get("/gitlab/commits", commitsRefresh)
}

// ActivityRefresh ...
func activityRefresh(c *echo.Context) error {

	c.JSON(200, activityEntries())

	return nil
}

// ProjectRefresh ...
func projectsRefresh(c *echo.Context) error {

	c.JSON(200, projects())

	return nil
}

// ProjectRefresh ...
func teamRefresh(c *echo.Context) error {

	c.JSON(200, team())

	return nil
}

// ProjectRefresh ...
func commitsRefresh(c *echo.Context) error {

	// c.JSON(200, commitsHour())
	// c.JSON(200, commitsDay())
	// c.JSON(200, commitsWeek())
	// c.JSON(200, commitsMonth())

	return nil
}

func projects() []*gl.Project {
	projects, err := gitlab.Projects()
	if err != nil {
		log.Fatal(err)
	}

	var relevantProjects []*gl.Project

	for _, project := range projects {
		if project.Namespace.Name == os.Getenv("GITLAB_NAMESPACE") {
			relevantProjects = append(relevantProjects, project)
		}
	}

	return relevantProjects
}

func activityEntries() []*gl.FeedCommit {
	activity, err := gitlab.Activity()
	if err != nil {
		log.Fatal(err)
	}

	return activity.Entries
}

func commits() []*gl.Commit {

	commits, err := gitlab.RepoCommits(fmt.Sprintf("%v", 122))
	if err != nil {
		log.Fatal(err)
	}

	commits, err = gitlab.RepoCommits(fmt.Sprintf("%v", 85))
	if err != nil {
		log.Fatal(err)
	}

	commits, err = gitlab.RepoCommits(fmt.Sprintf("%v", 84))
	if err != nil {
		log.Fatal(err)
	}

	commits, err = gitlab.RepoCommits(fmt.Sprintf("%v", 83))
	if err != nil {
		log.Fatal(err)
	}

	return commits
}

func team() []*gl.Member {

	team, err := gitlab.ProjectMembers(fmt.Sprintf("%v", 85))
	if err != nil {
		log.Fatal(errors.New("There was an issue retrieving team info from Gitlab"))
	}

	return team
}
